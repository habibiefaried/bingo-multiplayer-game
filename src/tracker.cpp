#include <iostream>
#include "ServerSocket.h"
#include "SocketException.h"
#include <string>
#include <pthread.h>
#include "Konversi.h"
#include "listpemain.h"

using namespace std;

ServerSocket server(5051); //listening socket server (global initialization)

void *Room(void *args)
{
	int i;
	int IDServer;
	
	ServerSocket client;
	string data,Jenis;
	string reply;
	string retr; //mengembalikan list of server
	char IPAddress[100];
	char IPDest[100];
	
	sockaddr_in sockaddress;
	
	i = (int)args;
	
	cout<<"Listening. . .("<<i<<")"<<endl;
	server.accept(client); //jika telah diterima oleh server
	ListPemain LP; //langsung memanggil kelas List of Pemain
  	
  	reply = "Anda pelanggan ke- " + IntToString(i) + ".\nAda " + IntToString(ListPemain::GetNList()) + " orang yang online saat ini.";
  	
  	sockaddress = server.GetSockAddress(); //Get sockaddr dari client 
  	strcpy(IPAddress,inet_ntoa(sockaddress.sin_addr));//mengambil IP address client yang terkoneksi
  	
  	try
	{
		client >> data; //receive dahulu data
		client << reply; //send
	}
	catch ( SocketException& e)
	{
		cout<<"Program menangkap eksepsi : "<<e.description()<<endl;
	};
	
	client >> Jenis; //listening untuk menerima kalimat terakhir (untuk server)
	
	LP.AddList(i,IPAddress,data,Jenis); //Memanggil Method AddList dengan parameter (ID,IP,Nama,Jenis)
	
	ClearScreen();
	ListPemain::PrintList(); //Memanggil method static printlist.
	//Data member kelas ListPemain adalah static juga
	
	cout<<"User bernama "<<data<<" baru saja login dari "<<IPAddress<<"."<<endl;
	
	if (Jenis == "Client") //jika client
	{
		List Temp;
		Temp = LP.GetList(); //Mengambil List of Server yang online
		while (Temp != NULL)
		{
			if (Temp->Info.Jenis == "Server")
			{
				retr = IntToString(Temp->Info.ID) + " -- " + Temp->Info.Nama;
				client << retr; //mengirimkan List of Alive Server kepada client
				client >> data; //menerima kembalian
			}
			Temp = Temp->Next;
		}
		client << "SELESAI"; //tanda selesai mengirim

		client >> data; //receive data tambahan
		IDServer = StringToInt(data); //ID server pilihan client
		if (LP.SearchIP(IDServer,IPDest)) //Memanggil method "SearchIP"
			client << IPDest;
		else
			client << "0"; // 0 merupakan invalid
	}
	
	try
	{
		client >> data; //menerima data dummy
	}
	catch (SocketException& e)
	{
		cout<<"Program menangkap eksepsi : "<<e.description()<<endl;
	}
	
	LP.DelList(i); //delete yang sudah keluar, sekaligus panggil DTor otomatis
	pthread_exit(NULL);
}

int main ( int argc, char *argv[] )
{
	pthread_t threads[10000]; //didefinisikan sebagai array of thread
	int i = 0,rc;
	int JmlThread;
	
	cout<<"Masukkan jumlah thread : "; cin>>JmlThread;
	
  // Create the socket
	i = 0;	
  	while (i<JmlThread)
	{
  		rc = pthread_create(&threads[i], NULL, Room, (void*)i); //memanggil thread
  		i++;
  		sleep(1); //setiap pembangunan thread, wait selama 1 detik
	}

	ClearScreen();
	cout<<"Listening . . ."<<endl;
    pthread_exit(NULL);
  	return 0;
}
