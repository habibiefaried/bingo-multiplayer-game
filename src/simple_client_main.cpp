#include <iostream>
#include <cstring>

using namespace std;

int RangeNilai;
int LebarBoard;

bool IsDone = false;
int TebakanSaya;
int TebakanLawan;

void Client_Game(string Name)
{
	char Pesan[100];
	//char IP[40];
	string IPReply;
	
	Bingo B;
	int col,row;

	string IPTracker;
	string Command;
		
	infotype reply,param1,param2;
	
	ClearScreen();
	
	/* Tracker */
	cout<<"Masukkan IP Address tracker : "; cin>>IPTracker;
	ClientSocket connect_socket (IPTracker,5051);
	
	connect_socket << Name; //memberikan Nama
	connect_socket >> reply; //menunggu reply dari server
	
	cout<<reply<<endl; //menuliskan reply dari server
	  	
	//cout<<"ketikkan sesuatu untuk memutuskan koneksi : "; cin>>S;
	connect_socket << "Client";
	cout<<"List Of Server yang aktif"<<endl;
	cout<<"===================================================="<<endl;
	cout<<"ID   Nama"<<endl;
	cout<<"===================================================="<<endl;
	while (reply != "SELESAI")
	{
		connect_socket >> reply; //menunggu reply server yang aktif
		
		if (reply != "SELESAI") //jika reply bukan flag "Selesai"
		{
			cout << reply << endl; //menuliskan reply dari server
			connect_socket << "OK"; //memberikan feedback "OK"
		}
	}
	cout<<"===================================================="<<endl;
	cout<<"Masukkan ID Server : "; cin >> Command;
	connect_socket << Command; 
	connect_socket >> IPReply; //dapat IP Address Jawaban 
	
	if (IPReply == "0")
		cout<<"Error!, server tidak ditemukan "<<endl;
	else
	{
		connect_socket << "QUIT"; //flags buat quit
		/* End of Tracker */

		Initialize(B);
		try
		{
			ClientSocket client_socket (IPReply,5050);
			try
			{
				/* Init Pesan */
				ClearScreen();
				strcpy(Pesan,"Connected to Server");
				//Istrcat(Pesan,IP);
		  		client_socket << Pesan;
		  		
		  		client_socket >> reply; //pesan balasan 
		  		cout<<reply<<endl;
		  		client_socket << ".";
		  		
		  		client_socket >> param1; //Menunggu konfigurasi lebar board dari server
		  		LebarBoard = StringToInt(param1);
		  		client_socket << ":";
		  		
		  		client_socket >> param2; //menunggu konfirmasi range nilai dari server
		  		RangeNilai = StringToInt(param2);
			
				/* End of Init Pesan */
			
				/* Initial Fasa */
				cout<<"Sedang menunggu lawan . . ."<<endl;
				client_socket >> reply;
				FasaMulai(B);		
				client_socket << "Selesai memasang board .";
				/* End of Initial Fasa */
			
				while (!IsDone)
				{
					ClearScreen();
					PrintBingo(B); //Print Board bingo
			
					cout<<"Menunggu pilihan lawan . . ."<<endl;
					client_socket >> reply; //recv reply (1)
				
					TebakanLawan =  StringToInt(reply); //enerjemahkan tebakan lawan
				
					if (SearchNumberLocation(B,TebakanLawan,col,row))
					//mencari tebakan lawan pada board bingo kita
					{
						B.a[row][col] = Marked;
					}
				
					//==================================================//
					/* Penentuan siapa yang menang */
					//==================================================//
				
					if (CountXAll(B) >= Menang)
					{
						ClearScreen();
						cout<<"Selamat, anda memenangkan pertandingan "<<endl;
						client_socket << Name;
						cout<<"Anda akan diredirect ke menu utama dalam 3 detik. . ."<<endl;
						sleep(3);
						break;
					}	
					else
						client_socket << "0";
					
					//==================================================//
					
					client_socket >> reply;
				
					if (reply != "0")
					{
						ClearScreen();
						cout<<"Maaf, anda kalah dalam pertandingan ini "<<endl;
						cout<<"Pemenangnya adalah "<<reply<<endl;
						cout<<"Anda akan diredirect ke menu utama dalam 3 detik. . ."<<endl;
						sleep(3);
						break;
					}
					//==================================================//
					/* End of Penentuan siapa yang menang */
					//==================================================//
				
					ClearScreen();
					PrintBingo(B);
				
					cout<<"Nilai yang anda minta : "; cin>>TebakanSaya;
			
					if (SearchNumberLocation(B,TebakanSaya,col,row)) 
					//mencari nomor tebakan sendiri di board
					{
						B.a[row][col] = Marked;
					}
				
					client_socket << IntToString(TebakanSaya); //send data (2)				
				}
			}
		  	catch ( SocketException& e)
		  	{
		  		cout<<"Program telah menangkap eksepsi : "<<e.description()<<"\n";
		  	}
		}
		catch ( SocketException& e )
		{
			std::cout << "Program telah menangkap eksepsi : " << e.description() << "\n";
		}
	}
}
