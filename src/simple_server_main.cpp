#include <iostream>
#include <cstring>

using namespace std;

/*
int RangeNilai;
int LebarBoard;
*/

void Server_Game(string Name)
{
	infotype data;
	infotype reply;
	infotype S;
	infotype Command;
	
	char Pesan[100];
	sockaddr_in Client;
	Bingo B;
	bool IsDone = false;
	int TebakanSaya;
	int TebakanLawan;
	int col,row;
	string IPTracker;
	
	ClearScreen();
	
	Initialize(B);
	
	/* Tracker */
	cout<<"Masukkan Lebar Board (harus ganjil) : "; cin>>LebarBoard; //lebar board bingo
	cout<<"Masukkan Range Nilai yang diperbolehkan : "; cin>>RangeNilai; //range nilai yang diperbolehkan

	cout<<"Masukkan IP Address tracker : "; cin>>IPTracker;
	ClientSocket client_socket (IPTracker,5051);
	
	client_socket << Name; //memberikan nama kepada server
	client_socket >> reply; //menunggu jawaban dari server
	
	cout<<reply<<endl; //menuliskan jawaban dari server
	  	
	client_socket << "Server"; //jenis program, apakah client atau server
	
	/* Tracker, End pada line 58 */
	
	ServerSocket new_sock;

	std::cout << "Listening @ "<<5050<<endl;
	try
    {
      // Create the socket
      	ServerSocket server(5050);
      	
  			server.accept(new_sock); //menerima message
  			client_socket << "QUIT"; //flags buat quit dari tracker
  				
			new_sock >> data; //receive dahulu data
			cout<<data<<endl;
	  		Client = server.GetSockAddress(); //mendapatkan sockaddr_in
	  		strcpy(Pesan,"Connected to ");
	  		strcat(Pesan,inet_ntoa(Client.sin_addr));
	  		
	  		new_sock << Pesan; //send
	  		new_sock >> data;
	  		
	  		new_sock << IntToString(LebarBoard); //mengirimkan lebar board kepada client
	  		new_sock >> data;
	  		
	  		new_sock << IntToString(RangeNilai); //mengirimkan batasan nilai kepada client
	  		
		  			  	
	  	try
	    {
	    	/* Init Pesan */
	  		ClearScreen();
	  		
	  		/* End of Init Pesan */
	  		
			/* Initial Fasa */
			FasaMulai(B);
			
			new_sock << "Selesai memasang board";
						
			cout<<"Menunggu lawan . . ."<<endl;
			
				new_sock >> data;
			
			/* End of Initial Fasa */
			
			while (!IsDone)
			{
				ClearScreen();
				PrintBingo(B);
				
				cout<<"Nilai yang anda minta : "; cin>>TebakanSaya;
				
				if (SearchNumberLocation(B,TebakanSaya,col,row)) //mencari nomor tebakan sendiri pada board 
				{
					B.a[row][col] = Marked;
				}
				
					new_sock << IntToString(TebakanSaya); //kirim data (1)
					/* Penentuan siapa yang menang */
					new_sock >> data; //recv data kemenangan
					if (data != "0")
					{
						ClearScreen();
						cout<<"Maaf, anda kalah dalam pertandingan ini "<<endl;
						cout<<"Pemenangnya adalah "<<data<<endl;
						cout<<"Anda akan diredirect ke menu utama dalam 3 detik. . ."<<endl;
						sleep(3);
						break;
					}
					//==========================================//
					if (CountXAll(B) >= Menang)
					{
						ClearScreen();
						cout<<"Selamat, anda memenangkan pertandingan "<<endl;					
						new_sock << Name;
						cout<<"Anda akan diredirect ke menu utama dalam 3 detik. . ."<<endl;
						sleep(3);
						break;
					}	
					else
						new_sock << "0";	
					/* End of Penentuan siapa yang menang */
					
				
				ClearScreen();
				PrintBingo(B); //Print board bingo
				
				cout<<"Menunggu pilihan lawan . . ."<<endl;
				
					new_sock >> data; //recv data (2)
				
					TebakanLawan = StringToInt(data);
				
					if (SearchNumberLocation(B,TebakanLawan,col,row))
					//mencari nomor tebakan lawan pada bingo
					{
						B.a[row][col] = Marked;
					}			
			}
		
	    }
	  	catch (SocketException& e)
	  	{
	  		cout<<"Program telah menangkap eksepsi : "<<e.description()<<"\n";
	  	}
    }
  	catch ( SocketException& e )
    {
      	std::cout << "Program telah menangkap eksepsi:" << e.description() << "\nQuit.\n";
    }
}
