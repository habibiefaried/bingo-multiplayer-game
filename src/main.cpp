#include <iostream>
#include <cstring>
#include "ClientSocket.h"
#include "SocketException.h"
#include "ServerSocket.h"
#include "SocketException.h"
#include "game.h"
#include "Konversi.h"
#include "simple_client_main.cpp"
#include "simple_server_main.cpp"


int main()
{
	int input;
	string Name;
	bool IsDone = false;
	
	ClearScreen();
	cout<<"Masukkan Nick anda : "; cin>>Name;
	
	while (!IsDone)
	{	
		ClearScreen();
		cout<<"MENU UTAMA"<<endl;
		cout<<"============================="<<endl;
		cout<<"1. Create Room "<<endl;
		cout<<"2. Join Room "<<endl;
		cout<<"3. Keluar "<<endl;
		cout<<"============================="<<endl;
		cout<<"Masukkan pilihan anda : "; cin>>input;
	
		if (input == 1)	Server_Game(Name);
		else if (input == 2) Client_Game(Name);
		else if (input == 3) IsDone = true;
		else cout<<"Pilihan salah"<<endl;
	}
	return 0;
}
