#ifndef _game_H_
#define _game_H_
#include <iostream>
#include <cstring>

extern int RangeNilai;
extern int LebarBoard;

const int Menang = 5;
const int Marked = -2;

typedef struct
{
	int a[30][30];
	int kepunyaan; //1: server atau 2: client
	int IsMenang; //1 : menang, 0 : blom menang
}Bingo;

//Fungsi-Fungsi
/* Prosedur gaming */
void Initialize(Bingo &B);
void PrintBingo(const Bingo &B);
void IsiBingo(Bingo &B, int row, int col, int Nilai);
void FasaMulai(Bingo &B);
bool SearchNumberLocation(const Bingo &B, int Tebak, int &col, int &row);

/*Menghitung X*/
bool XHorizontal(const Bingo &B, int row);
bool XVertikal(const Bingo &B, int col);
bool XDiagonalKanan(const Bingo &B);
bool XDiagonalKiri(const Bingo &B);
int CountXAll(const Bingo &B);

/* Semua Boolean */
bool IsInputValid(int row, int col, int Nilai);
bool IsFull(Bingo &B);
//End Of Fungsi-Fungsi




#endif
