// Implementation of the ServerSocket class

#include "ServerSocket.h"
#include "SocketException.h"

using namespace std;

ServerSocket::ServerSocket ( int port )
{
  if (!Socket::create()) //Ketika create tidak bisa
    {
      throw SocketException("Could not create server socket."); //Throw socket
    }

  if (!Socket::bind(port)) //ketika tidak bisa bind
    {
      throw SocketException ("Could not bind to port."); //throw error message
    }

  if (!Socket::listen())
    {
      throw SocketException ( "Could not listen to socket." ); //throw error message
    }

}

ServerSocket::~ServerSocket()
{}


const ServerSocket& ServerSocket::operator<<( const infotype& s ) const
{
  	if (!Socket::send(s)) //ketika tidak bisa kirim
    {
      throw SocketException ( "Could not write to socket." );
    }
	return *this;
}


const ServerSocket& ServerSocket::operator >> ( infotype& s ) const
{
  	if (!Socket::recv(s)) //ketika receive data dari client
    {
     throw SocketException ( "Could not read from socket." );
    }
	return *this;
}

void ServerSocket::accept(ServerSocket& sock)
{
  	if (!Socket::accept(sock))
    {
      throw SocketException ("Could not accept socket.");
    }
}
